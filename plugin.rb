# name: Category Link Redirect
# about: Redirect users who click on a Discourse category in Discourse to /discover/ with the appropriate topic
# version: 1.0
# authors: Ivan RL
# url: https://ivanrlio@bitbucket.org/ivanrlio/discourse-category-link-redirect.git


